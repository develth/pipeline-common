## `monolith-gem-config` component

The component generates jobs for gems vendored in the main GitLab project under `gems/`.

### Inputs

#### `gem_name`

The name of the gem, i.e. if the gem is located at `gems/gitlab-rspec`, `gem_name` should be set to `gitlab-rspec`.
